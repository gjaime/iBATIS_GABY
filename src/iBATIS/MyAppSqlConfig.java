package iBATIS;
import java.io.Reader;
import com.ibatis.common.resources.*;
import com.ibatis.sqlmap.client.*;

public class MyAppSqlConfig {
	//public MyAppSqlConfig {
	
		private static final SqlMapClient sqlMap;
		static {
		try {
		String resource = "iBATIS/SqlMapConfig.xml";
		//String resource = "com/ibatis/example/sqlMap-config.xml";
		Reader reader = Resources.getResourceAsReader (resource);
		sqlMap = SqlMapClientBuilder.buildSqlMapClient(reader);
		} catch (Exception e) {
			
		// Si hay un error en este punto, no importa cual sea. Ser� un error irrecuperable del cual
		// nos interesar� solo estar informados.
		// Deber�s registrar el error y reenviar la excepci�n de forma que se te notifique el
		// problema de forma inmediata.
			
		e.printStackTrace();
		throw new RuntimeException ("Error initializing MyAppSqlConfig class. Cause: " + e);
		}
		}
		public static SqlMapClient getSqlMapInstance () {
		return sqlMap;
		}		
}
